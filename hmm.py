import time
import random
import itertools

def bitProb(p, bit): # p**bit * (1-p)**(1-bit)
	if bit == 1:
		return p
	if bit == 0:
		return 1-p 

def pathProb(path,word,stateProbOf1,transitProb):
	prob = 1

	for i in range(0, len(path) - 1):
		state = path[i]
		prob = prob * bitProb(stateProbOf1[state],word[i])
		state1 = path[i]
		state2 = path[i+1]
		prob = prob * transitProb[state1][state2]

	state = path[len(path) - 1]
	prob = prob * bitProb(stateProbOf1[state],word[len(path) - 1])
	return prob



def wordProb(word,stateProbOf1,transitProb,states):
	return sum(
		[
			pathProb(
				(0,) + pathEnd, word, stateProbOf1, transitProb
			) for pathEnd in itertools.product((i for i in range(0, states)), repeat=len(word)-1)
		]
	)

def report(stateProbOf1,transitProb,states):
	for anyWord in itertools.product((i for i in range(0, alphabetSize)), repeat=len(word)):
		prob = wordProb(anyWord,stateProbOf1,transitProb,states)
		print "Probability of " + str(anyWord) + " is " + str(round(prob,3))


def randomTransitProb(states):
	bad = True
	while bad:
		transitProb = [[round(random.random(), 10) for i in range(0, states)] for j in range(0, states)]
		bad = False
		for j in range(0, states):
			if sum(transitProb[j]) > 1:
				bad = True
	return transitProb

def randomStateProbOf1(states):
	stateProbOf1 = [round(random.random(), 10) for i in range(0, states)]
	return stateProbOf1


def randomTransitProbNear(states,transitProb,zoom): # something like simulated annealing
	bad = True
	count = 0
	zoom = zoom * 5
	while bad and count < 100:
		randomTransitProb = [[round(transitProb[j][i] + random.random()/zoom, 10) for i in range(0, states)] for j in range(0, states)]
		bad = False
		for j in range(0, states):
			if sum(randomTransitProb[j]) > 1:
				bad = True
		count += 1
		if count >= 10: # give up before taking too much time
			return transitProb
	#print "which was close to: " + str(randomTransitProb)
	return randomTransitProb

def randomStateProbOf1Near(states, stateProbOf1,zoom): # something like simulated annealing
	return [
		round(random.random()/zoom + stateProbOf1[i], 10) for i in range(0, states)
	]

def randomEstimateHMMcomplexity(word, states):
	maxProb = 0
	for i in range(1,2000000):
		x = random.random()
		y = random.random()
		transitProb = {
			(0,0): 1-x,
			(0,1): x,
			(1,0): y,
			(1,1): 1-y
		}
		p = random.random()
		q = random.random()
		stateProbOf1 = {
			0: p,
			1: q
		}
		now = wordProb(word,stateProbOf1,transitProb)
		if (now>maxProb):
			maxProb=now
			print "wordProb(" + str(p) + ", " + str(q) + ", " + str(x) + ", " + str(y) + ") = " + str(maxProb)
			report(stateProbOf1,transitProb,states)
	print "Done with random trials."

def getTuples(states, denom):
	if states == 4:
		tuples = [
			(
				i/float(denom),
				j/float(denom),
				k/float(denom),
				1-((i+j+k)/float(denom))
			) for i in range(0, denom+1) for j in range(0, denom+1-i) for k in range(0, denom+1-i-j)
		]
	if states == 3:
		tuples = [
			(
				i/float(denom),
				j/float(denom),
				1-((i+j)/float(denom))
			) for i in range(0, denom+1) for j in range(0, denom+1-i)
		]
	if states == 2:
		tuples = [
			(
				i/float(denom),
				1-(i/float(denom))
			) for i in range(0, denom+1)
		]
	if states == 1:
		tuples = [
			(1,)
		]
	return tuples

def HMMcomplexity(word, states, edges):
	maxWordProb = 0
	maxPathProb = 0
	annealParam = 5
	bestStateProbOf1 = randomStateProbOf1(states)
	bestTransitProb = randomTransitProb(states)

	ii = 0
	denom = edges
	tuples=getTuples(states, denom)
	for systematicTransitProb in itertools.product(tuples, repeat=states):
		for systematicStateProbOf1 in itertools.product([i/float(denom) for i in range(0,denom+1)],repeat=states):
			ii += 1
			#if ii % 2 == 0:
			#	if ii % annealParam == 0:
			#		stateProbOf1 = randomStateProbOf1(states)
			#		transitProb = randomTransitProb(states)
			#	else:
			#		stateProbOf1 = randomStateProbOf1Near(states,bestStateProbOf1,ii % annealParam)
			#		transitProb = randomTransitProbNear(states,bestTransitProb, ii % annealParam)
			#else:
			stateProbOf1 = systematicStateProbOf1
			transitProb = systematicTransitProb
			basicData = [
				pathProb(
					(0,) + pathEnd, word, stateProbOf1, transitProb
				) for pathEnd in itertools.product((i for i in range(0, states)), repeat=len(word)-1)
			]
			nowWordProb = sum(basicData)
			nowPathProb = max(basicData)
			if (nowWordProb>maxWordProb or nowPathProb>maxPathProb):
				if (nowWordProb>maxWordProb):
					print
					print "NEW MAX WORD PROBABILITY: " + str(nowWordProb)
					maxWordProb=nowWordProb
				if (nowPathProb>maxPathProb):
					print "New max path probability: " + str(nowPathProb)
					maxPathProb=nowPathProb
					if maxPathProb == 1:
						return
				if maxPathProb < 1/2 and maxWordProb >= 1/2:
					print "Words vs. paths structure function difference found"
					raise SystemExit
				if maxPathProb < 1/4 and maxWordProb >= 1/4:
					print "Words vs. paths structure function difference found"
					raise SystemExit
				#report(stateProbOf1,transitProb,states)


alphabetSize = 2
edges = 12
states = 3

word = (0,1,1,0,1,0)
for edges in range(1, 10):
	print "Edges: " + str(edges)
	HMMcomplexity(word, states=states, edges=edges)
