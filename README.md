# Few paths, fewer words: model selection with automatic structure functions #

The purpose of this repository is to make available Python proofs of the theorems stated as "proved by computer" in the paper [Few paths, fewer words: model selection with automatic structure functions](https://arxiv.org/abs/1608.01399):

### Theorem 6 ###
A_N^{path}(010111010)=5.

This is checked by running

```
#!python

import automatoncomplexity
xseq = (0, 1, 0, 1, 1, 1, 0, 1, 0)
s = SimpleStrings(q = 5, n = len(xseq), b = 2, useFiles = False, m = 0)
s.complexity(xseq)

```


### Theorem 14 ###
There is no binary word $x$ with $|x|\le 10$ and $A^{word}_N(x) \ne A^{\path}_N(x)$.

### Theorem 16 ###
Let $x=001011$. Then $h_x^{*path}(|x| - 2) = 3$.

This is obtained in the following style:
```
#!python

import automatoncomplexity
import automatonreports

xseq = (0,0,1,0,1,1)
automatonreports.displayStructureFunction(
	xseq,
	b = 2,
	printStateSequences    = True,
	printStructureFunction = True,
	wordBased              = False,
)
```


### Theorem 18 ###
Let $x=000010000$. Then $h^{*path}_x(1)=5$.

### Theorem 23 ###
Let $x=01111011011$.
In both the path-counting mode and the deterministic mode the optimal number of states for $x$ is 3.
The only optimal state sequence for $x$ in the path-counting mode is 012120120120, giving $m=2$ and $p$-value $0.04$.
The only optimal state sequence for $x$ in the deterministic mode is 012020120120, giving $m=4$ and $p$-value $0.30$.

### Theorem 25 ###
The optimal number of states for $x=0110001000$ in the path-counting mode is 4, corresponding to $m=2$ and a $p$-value of 0.79.
The optimal number of states for $x=0110001000$ in the word-counting mode is 2, corresponding to $m=7$ and a $p$-value of 0.6.

The main "engine" for the computations is the automatoncomplexity.py class file, which is used by automatonreports.py to generate reports on various statistics. Concrete computations are then run using automatonSandbox.py. 

Data produced this way was saved in
* dualStructureFunctionData.py
* dualStructureFunctionDataDeterministic.py
* dualStructureFunctionDataWordCounting.py

That data is then processed by
* words-vs-paths-structure-function.py
to obtain Theorem 23 and Theorem 25.

### Theorem 30 ###
Let $\F=\{0110, 1111\}$. For any NFA $M$ such that $L(M)\cap \{0,1\}^4=\F$, $M$ has at least 3 states, and
if $M$ has 3 states then $M$ has at least 3 accepting paths of length 4.