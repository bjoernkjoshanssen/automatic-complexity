import os
import time
import numpy
import random
import sys
import itertools
import automatoncomplexity
import automatonreports

from automatoncomplexity import SimpleStrings

from automatonreports import checkOfThePathsVsStringsConjecture
from automatonreports import checkSimplicity
from automatonreports import checkStringWithinPathsVsWordsConjecture
from automatonreports import displayStructureFunction
from automatonreports import flexibleStructureFunction
from automatonreports import generateRandomString
from automatonreports import getInverseStructureFunction
from automatonreports import slowincrease
from automatonreports import structureFunctionPoint

#getInverseStructureFunction((0,0,0,1,0,1,0),verbose=True)
#raise SystemExit


#xseq = (0,0,1,2,3,1,3,1,2,2,1,3) # 8
#xseq = (0,0,1,2,3,1,3,1,4,2,1,3) # 8
#xseq = (0,5,1,2,3,5,0,5,4,2,1,3) # 9
#xseq = (0,5,1,2,3,6,7,8,4,9,1,3) # 9
xseq = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19) # 
displayStructureFunction(
	xseq,
	b = 20,
	printStateSequences    = True,
	printStructureFunction = True,
	#printStats             = True,
	#checkDeterminism       = True,
	wordBased              = False,
	endStateIsStartState   = True,
	mMax = 0
)
raise SystemExit

#xseqStem = (0, 1, 0, 0, 0, 1, 1, 1)
#xseqStem = (0, 1, 0, 0, 0, 1, 1, 1, 1, 1)
#xseqStem = (0, 1, 1)
xseqStem = (0,)
for xseqTail in itertools.product((0, 1, 2), repeat=5):
	xseq = xseqStem + xseqTail
	displayStructureFunction(
		xseq,
		b = 3,
		printStateSequences    = True,
		printStructureFunction = True,
		#printStats             = True,
		#checkDeterminism       = True,
		wordBased              = False,
		endStateIsStartState   = True
	)
raise SystemExit


printStateSequences = False
printStats = False
checkDeterminism = False
wordBased = True


xseq = (0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0)

displayStructureFunction(
	xseq,
	printStateSequences    = True,
	printStructureFunction = True,
	printStats             = True,
	checkDeterminism       = False,
	wordBased              = True,
	mMax                   = 0,
	verbose                = True
)
raise SystemExit





xseq = (0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0)

displayStructureFunction(
	xseq,
	printStateSequences    = True,
	printStructureFunction = True,
	printStats             = False,
	checkDeterminism       = False,
	wordBased              = True
)
displayStructureFunction(
	xseq,
	printStateSequences    = True,
	printStructureFunction = True,
	printStats             = False,
	checkDeterminism       = False,
	wordBased              = False
)









for n in range(10, 10 + 1):
	for xseq in itertools.product((0,1),repeat=n):
		if xseq[0] == 0:
			displayStructureFunction(
				xseq,
				printStateSequences = False,
				printStructureFunction = False,
				printStats = False,
				checkDeterminism = False,
				wordBased = True
			)
raise SystemExit







for n in range(7, 11):
	bb = 4 # or bb = n
	for xseq in itertools.product((i for i in range(0, bb)),repeat=n):
		if slowincrease(xseq, bb):
			#if xseq == (0, 1, 2):
			#print "PATHY"
			pathy = flexibleStructureFunction(xseq, b=bb, verbose=False, checkDeterminism=False, wordBased=False, mMax=0)
			#print "WORDY"
			wordy = flexibleStructureFunction(xseq, b=bb, verbose=False, checkDeterminism=False, wordBased=True, mMax=0)
			if wordy != pathy:
				print str(xseq) + ", word-based: " + str(wordy) + ", path-based: " + str(pathy)
			else:
				print str(xseq) + ", " + str(wordy)
raise SystemExit
# What has been checked for the paths vs words conjecture: (1 = checked, (1) = recently checked, ! = actively checking, . = irrelevant, 0 = not checked yet)
# n
#10 ! 0 0 0 0 0 0 0 0
# 9(1)0 0 0 0 0 0 0 .
# 8 1 0 0 0 0 0 0 . .
# 7 1 1(1)0 0 0 . . . (n=7, b=3 took an hour or so and included some m=1 examples)
# 6 1 1 1 1(1). . . . (so length <= 6 definitely has no examples...)
# 5 1 1 1 1 . . . . .
# 4 1 1 1 . . . . . .
# 3 1 1 . . . . . . .
# 2 1 . . . . . . . .
# 1 . . . . . . . . .
# b=2 3 4 5 6 7 8 9 A



for xseq in itertools.product((0,1),repeat=8):
	if xseq[0] == 0:
		flexibleStructureFunction(xseq, b=2, verbose=False, wordBased=False)
raise SystemExit

#xseq = (0,0,0,1,0,1,0,1,1,0,1,0,0,0,0,1,1,0,0,1,0,0,1,1,1,1,1,0,1,1,1) #the supercomplex one A_D\ge A_N+2
xseq = (0,1,0,0,0,1,1,1,1,0,1,0,1,1,0) #one of the LFSR strings
n = 15
q = 7
#xseq = (0,1,0,0,0,1,1,1,1,0,1,0) #
#n = 12
#q = 6
#xseq = (0,1,0,0,0,1,1,1,1,0,1) #
#n = 11
#q = 5

xseq = (0,1,1,1,0,0,0) # from Few paths, fewer words paper automatic-lics.pdf, Theorem 7
n = len(xseq)
q = 3 # from Few paths, fewer words paper automatic-lics.pdf, Theorem 7
b = 2
m = 2 # from Few paths, fewer words paper automatic-lics.pdf, Theorem 7
#checkStringWithinPathsVsWordsConjecture(xseq, n=n, q=q, b=b, m=m)




#print str(wordBasedStructureFunctionPoint((0,0), n=2, q=1, b=2, m=0)) #should be True

#wordBasedStructureFunction(xseq, b=2)
# Plan:
# Upload word based structure function to Google
# Implement a comparison at Structure Function Calculator
# Find an example where the p-value is minimized at a different point.
# ... but first find the optimal p-value for h^{word} just for 0111000.
for xseq in itertools.product((0, 1), repeat=10):
	#print
	if xseq[0] == 0:
		wordBasedStructureFunction(xseq, b=2, verbose=True)

raise SystemExit

for xseq in itertools.product((0,1), repeat=13):
	if xseq[0] == 0:
		#print str(xseq)
		getInverseStructureFunction(xseq) # make a word-based version of this. 
		#how does it avoid printing the same seq several times???
		#by making allDone=True when m==0
raise SystemExit


for n in range(0, 8):
	print
	print "LENGTH " + str(n)
	for xseq in itertools.product((0, 1), repeat=n):
		wordBasedStructureFunction(xseq)
#

raise SystemExit


import cProfile
cProfile.run('checkOfThePathsVsStringsConjecture(n=11,q=2,b=2)')#11,2,2 was fast, 172 seconds. 7,3,7 was OK (10 hours)
cProfile.run('checkOfThePathsVsStringsConjecture(n=11,q=3,b=2)')
cProfile.run('checkOfThePathsVsStringsConjecture(n=11,q=4,b=2)')
cProfile.run('checkOfThePathsVsStringsConjecture(n=11,q=5,b=2)')
raise SystemExit

xseq = (0,0,0,1,0,1,0,1,1,0,1,0,0,0,0,1,1,0,0,1,0,0,1,1,1,1,1,0,1,1,1)
for mm in range(0, 1):
	for myq in range(17, 18): #10 to 15
		print
		print "Testing q=" + str(myq)
		print "Testing " + str(xseq) + " cyclically"
		print
		for shift in range(0, 31): #(0,31), i.e. 0 to 30, as 31 is equivalent to 0
			t0 = time.time()
			print "Testing",
			print str(xseq),
			print "for complexity at most " + str(myq) + " with mm=" + str(mm)
			#print len(xseq)
			s = SimpleStrings(q = myq, n = len(xseq), b = 2, useFiles = False, m = mm)
			s.complexity(xseq, checkDeterminism=True)
			t1 = time.time()
			total = t1-t0
			print "(" + str(round(total/60,2)) + " minutes)"
			xseq = (xseq[30],) + xseq[0:30]
raise SystemExit


xseq = (1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0)

for mm in range(0, 1):
	for myq in range(16, 17): #10 to 15
		print
		print "Testing q=" + str(myq)
		print "Testing " + str(xseq) + " cyclically"
		print
		for shift in range(0, 31): #(0,31), i.e. 0 to 30, as 31 is equivalent to 0
			t0 = time.time()
			print "Testing",
			print str(xseq),
			print "for complexity at most " + str(myq) + " with mm=" + str(mm)
			#print len(xseq)
			s = SimpleStrings(q = myq, n = len(xseq), b = 2, useFiles = False, m = mm)
			s.complexity(xseq, checkDeterminism=False)
			t1 = time.time()
			total = t1-t0
			print "(" + str(round(total/60,2)) + " minutes)"
			xseq = (xseq[30],) + xseq[0:30]
raise SystemExit




sfw = [
	[
		(),
		[
			(0,)
		]
	],
	[
		(0,),
		[
			(0,0)
		]
	],
	[
		(0,0),
		[
			(0,0,0)
		]
	]
]
print sfw[1][1][1]
print ''.join(map(str, (0, 1, 0)))
raise SystemExit


if False:
	xseq = (0,0,1,1,1,1,0)
	myq = 4
	mm = 0
	checkDeterminism = False
	s = SimpleStrings(
		q = myq, n = len(xseq), b = 2, useFiles = False, m = mm, isItTimeToReturn=False, returnWhenFound=False
	)
	print "..."
	s.complexity(xseq, checkDeterminism=checkDeterminism)
	raise SystemExit





#s = SimpleStrings(q=5, n=19, b=2, useFiles=False, m=0, approxLength=True)
#s.allComplexityBounds(checkDeterminism=False)
#print str(s.numSimple())
#raise SystemExit

#import cProfile
#myq=9
#xseq=(0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,1,0,0,1,0,1)
#mm=0
#cProfile.run('s = SimpleStrings(q = len(xseq)/2, n = len(xseq), b = 2, useFiles = False, m = 0, isItTimeToReturn=False, returnWhenFound=False, checkResetDeterminism=False)')
#cProfile.run('s.complexity(xseq, checkDeterminism=True)')
#raise SystemExit

#xseq = (0,)*35 + (1,) + (2,3,4,5,6,7)*4
###xseq = (0,)*19 + (1,) + (2,3,4,5,6,7)*3
#xseq = (0,)*19 + (0,) + (1,2,3,4,5,6)*3
#xseq = (0,)*19 + (1,2,3,4,5,6)*3

#xseq = (0,)*38#works
#xseq = (0,)*27#doesn't work
#xseq = (0,)*13#didn't work
#xseq=(0,)*30#didn't work
#minQ = 11
#minQ = 5
#xseq = (0,)*13
xseq = (0,)*5
minQ = 2
mm = 0
checkDeterminism = False
for myq in range(minQ, minQ + 5):
	t0 = time.time()
	print
	print "Testing",
	print str(xseq),
	print "for complexity at most " + str(myq) + " with mm=" + str(mm)
	#print len(xseq)
	s = SimpleStrings(
		q = myq, n = len(xseq), b = 2, useFiles = False, m = mm, isItTimeToReturn=False, returnWhenFound=False, checkResetDeterminism=False, approxLength=True
	)
	print "..."
	s.complexity(xseq, checkDeterminism=checkDeterminism)
	t1 = time.time()
	total = t1-t0
	print "(" + str(round(total/60,2)) + " minutes)"
raise SystemExit




"""
(1,1,1,0,0,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,0,0,0,1,1), is a non relatively complex
(1,1,1,0,0,1,1,0,1,0,0,1,0,0,0,0,1,0,1,0,1,1,1,0,1,1,0,0,0,1,1) one modified to replace 01000 0101 by 1^9



(1,1,1,0,0,1,1,0,1,0,2,2,2,2,2,2,2,2,2,2,2,1,1,0,1,1,0,0,0,1,1), is a non relatively complex
(1,1,1,0,0,1,1,0,1,0,0,1,0,0,0,0,1,0,1,0,1,1,1,0,1,1,0,0,0,1,1) one modified to replace 01000 010101 by 2^{11}
Let us check whether it is a counterexample to the paths vs words former-conjecture that A^path=A^word:
We know that we can come down to myq=15, mm=1 (two paths), and one word:
state sequence
(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 10, 11, 12, 13, 10, 11, 12, 6, 7, 8, 9, 3, 4, 5, 0, 1, 2)
  (1, 1, 1, 0, 0, 1, 1, 0, 1, 0,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  1, 1, 0, 1, 1, 0, 0, 0, 1, 1)
Can we do that well with just one path?
"""
xseq = (1,1,1,0,0,1,1,0,1,0,2,2,2,2,2,2,2,2,2,2,2,1,1,0,1,1,0,0,0,1,1)
mm = 0
checkDeterminism = False
for myq in range(1, 16):
	t0 = time.time()
	print
	print "Testing",
	print str(xseq),
	print "for complexity at most " + str(myq) + " with mm=" + str(mm)
	#print len(xseq)
	s = SimpleStrings(
		q = myq, n = len(xseq), b = 3, useFiles = False, m = mm, isItTimeToReturn=False, returnWhenFound=True
	)
	print "..."
	s.complexity(xseq, checkDeterminism=checkDeterminism)
	t1 = time.time()
	total = t1-t0
	print "(" + str(round(total/60,2)) + " minutes)"
raise SystemExit
""" Well, that didn't actually work. The diagnosis may be that the 2^{11} block was too long relatively speaking.
Try with another example where the 2-block does not dominate as much."""


lfsrStrings = (
	(1,0,0,0,0, 1,1,1,0, 0,1,1,0,1,1,1,1,1,0,1,0,0,0,1,0,0,1,0,1,0,1), #the right one
	(1,1,1,0,0,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,0,0,0,1,1),
	(0,0,0,1,0,1,0,1,1,0,1,0,0,0,0,1,1,0,0,1,0,0,1,1,1,1,1,0,1,1,1),#the supercomplex one A_D\ge A_N+2
	(1,0,0,0,0,1,0,1,0,1,1,1,0,1,1,0,0,0,1,1,1,1,1,0,0,1,1,0,1,0,0),
	#(0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1),
	#(1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0), not ubercomplex (A_N+2)
	(1,0,0,0,0, 1,1,0,0, 1,0,0,1,1,1,1,1,0,1,1,1,0,0,0,1,0,1,0,1,1,0),

	#(0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0), #the temporary one
	#(1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1), #another temp one 
	#(1,0,0,0,0,1,0,0,1,0,1,1,0,0,1,1,1,1,1,0,0,0,1,1,0,1,1,1,0,1,0),
	#(1,0,0,0,0,1,0,1,1,0,1,0,1,0,0,0,1,1,1,0,1,1,1,1,1,0,0,1,0,0,1),
	#(1,0,0,0,0,1,1,0,1,0,1,0,0,1,0,0,0,1,0,1,1,1,1,1,0,1,1,0,0,1,1)
)

# Currently running:
lfsrStrings15 = (
	(1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0),
)

fun = (
	(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
)
#structureFunctionPoint(myStrings=lfsrStrings, mStart=1, mEnd=1, qStart=15, qEnd=15, shiftStart=0, shiftEnd=30, checkDeterminism=False)
#structureFunctionPoint(myStrings=lfsrStrings, mStart=0, mEnd=0, qStart=18, qEnd=18, shiftStart=0, shiftEnd=30, checkDeterminism=True)

structureFunctionPoint(myStrings=fun, mStart=0, mEnd=0, qStart=10, qEnd=13, shiftStart=0, shiftEnd=0, checkDeterminism=False)

raise SystemExit


randomString15 = (
	(0,1,0,0,0,1,0,1,1,0,0,1,0,0,0),
)

#structureFunctionPoint(myStrings=lfsrStrings15, mStart=0, mEnd=4, qStart=8, qEnd=8, shiftStart=0, shiftEnd=0)

#structureFunctionPoint(myStrings=randomString15, mStart=0, mEnd=15, qStart=2, qEnd=2, shiftStart=0, shiftEnd=0)

#getInverseStructureFunction((0,1,0,0,0,1,0,1,1,0,0,1,0,0,0))




xseq = (
	random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1),
	random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1),
	random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1)
)

#xseq = (
#	0,0,0,0,0,
#	0,0,0,1,1,
#	1,1,1,1,1
#)

xseq = (0,1,1,0,1,0,0,1,1,0,0,1)

getInverseStructureFunction(xseq)
os.system("clear")
raise SystemExit

for mm in range(2, 31):
	for myq in range(5, 6): #10 to 15
		print
		print "Testing q=" + str(myq)
		for baseSeq in lfsrStrings:
			xseq = baseSeq
			print
			print "Testing " + str(baseSeq) + " cyclically"
			print
			for shift in range(0, 1): #(0,31), i.e. 0 to 30, as 31 is equivalent to 0
				t0 = time.time()
				print "Testing",
				print str(xseq),
				print "for complexity at most " + str(myq) + " with mm=" + str(mm)
				#print len(xseq)
				s = SimpleStrings(q = myq, n = len(xseq), b = 2, useFiles = False, m = mm)
				s.complexity(xseq, checkDeterminism=False)
				t1 = time.time()
				total = t1-t0
				print "(" + str(round(total/60,2)) + " minutes)"
				xseq = (xseq[30],) + xseq[0:30]
raise SystemExit
#xseq=(0,0,1,0,0)
#s = SimpleStrings(q = 2, n = len(xseq), b = 2, useFiles = False, m = 0)
#xseq = (1,0,0,0,0,0,0,1,0,0,1,1,1,1,1,1,1,1,0,0,1,0,1)
#xseq = (1,0,1,0,0,1,1,1,1,1,1,1,1,0,0,1,0,0,0,0,0,0,1)

#LFSR generated:
#xseq= (1,0,0,0,0,1,0,0,1,0,1,1,0,0,1,1,1,1,1,0,0,0,1,1,0,1,1,1,0,1,0) complex
#xseq = ( 0,0,0,0,1,0,0,1,0,1,1,0,0,1,1,1,1,1,0,0,0,1,1,0,1,1,1,0,1,0,1) complex
#xseq = (    0,0,0,1,0,0,1,0,1,1,0,0,1,1,1,1,1,0,0,0,1,1,0,1,1,1,0,1,0,1,0) complex

#Randomly generated:
#q=10  47 seconds... q=11  120 secs... q=12 332 secs...
#q=13 835 seconds or 14 minutes... q=14 2775 secs or 46 minutes... q=15 5437 secs.
#xseq = (0,1,0,0,0,1,0,1,1,0,0,1,0,0,0,0,0,1,1,0,1,1,1,1,0,0,0,1,1,1,0)

xseq = (0,1,1,0,0,0,1,0,1,0,1,0,1,1,1,1,1,1,0,0,1,0,1,1,1,0,0,1,1,1,1)
#q=10 39 secs, q=11 106 secs, q=12 256 secs, q=13 667 secs, q=14 1668 secs, q=15 secs

print len(xseq)
s = SimpleStrings(q = 15, n = len(xseq), b = 2, useFiles = False, m = 1)
s.complexity(xseq, checkDeterminism=False)
raise SystemExit



xseq = (0,0,1)
s = SimpleStrings(q = 3, n = len(xseq), b = 2, useFiles = False, m = 1)
s.complexity(xseq, checkDeterminism=True)
raise SystemExit



#For xseq = (0,0,1,1,0,1,0,1,1,0,1,1,0,1,1,0) we get the same except h_x^-1(3)=9 vs. 10.
# For xseq = (0,1,1,0,0,1,1,1,0) they agree except on h_x^-1(0).
# xseq = (0,1,0,1,1,1,0,0,1,1,0,0,1,1,0,0,0,1,1,1) too slow...
xseq = (0,1,0,1,1,1,0,0,1,1,0,0,1,1,0)
xseq = (1,0,1,1,1,0,0,1,1,0,1,1,0,1,0)
xseq = (1,1,1,0,1,0,1,0,0,1,1,1,0,0,0)
xseq = (
	random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1),
	random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1),
	random.getrandbits(1), random.getrandbits(1), random.getrandbits(1), random.getrandbits(1)
)
print "NFA:"
getInverseStructureFunction(xseq, checkDeterminism=False) #"pass in checkDeterminism to that and to checkSimplicity
print "DFA:"
getInverseStructureFunction(xseq, checkDeterminism=True) #"pass in checkDeterminism to that and to checkSimplicity
raise SystemExit

print "DFA:"
xseq = (0,1,1,1,0,0,0)
s = SimpleStrings(q = 2, n = len(xseq), b = 2, useFiles = False, m = 3)
s.complexity(xseq, checkDeterminism=True)
print "NFA:"
s = SimpleStrings(q = 2, n = len(xseq), b = 2, useFiles = False, m = 3)
s.complexity(xseq, checkDeterminism=False)
raise SystemExit




print "With determinism:"
s = SimpleStrings(q = 3, n = 5, b = 3, useFiles = False, m = 0)
s.allComplexityBounds(checkDeterminism=True)
print str(s.numSimple())
raise SystemExit


print "With determinism:"
s = SimpleStrings(q = 11, n = 18, b = 2, useFiles = False, m = 0)
s.allComplexityBounds(checkDeterminism=True)
print str(s.numSimple())
raise SystemExit




xseq = (0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0)
s = SimpleStrings(q = 10, n = len(xseq), b = 2, useFiles = False, m = 0)
s.complexity(xseq, checkDeterminism=True)
#print
raise SystemExit



print "With determinism:"
s = SimpleStrings(q = 11, n = 18, b = 2, useFiles = False, m = 0)
s.allComplexityBounds(checkDeterminism=True)
print str(s.numSimple())
raise SystemExit


#print "With nondeterminism:"
#s = SimpleStrings(q = 8, n = 15, b = 2, useFiles = False, m = 0)
#s.allComplexityBounds(checkDeterminism=False)
#print str(s.numSimple())
print "With determinism:"
s = SimpleStrings(q = 10, n = 17, b = 2, useFiles = False, m = 0)
s.allComplexityBounds(checkDeterminism=True)
print str(s.numSimple())
raise SystemExit


xseq=(0,0,0,1,1,0,1)
#print "Without determinism: "
#s = SimpleStrings(q = 5, n = len(xseq), b = 2, useFiles = False, m = 0)
#s.complexity(xseq, checkDeterminism=False)
#print
#print
print "With determinism: "
s = SimpleStrings(q = 5, n = len(xseq), b = 2, useFiles = False, m = 0)
s.complexity(xseq, checkDeterminism=True)
#getInverseStructureFunctionForQEqualThree(
#	(0,0,0,1,0,0,1,0,1,1,0,1,1,1)
#)
raise SystemExit


checkOfThePathsVsStringsConjecture()
raise SystemExit

print time.localtime(time.time())
