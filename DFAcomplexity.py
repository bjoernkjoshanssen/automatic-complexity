from datetime import datetime
import itertools
import numpy
qfixed = 7
#print "The number of states considered is q = " + str(qfixed)


#x = "01011111"
#x = (0,0,1,0,1,0,1,1)
#print "The string considered is x = " + str(x)


def slowishIncrease(d):
	for j in range(0, len(d)):
		if d[j] > j + 1:
			return False
	return True

def generateAllBinaryStrings(n):
	strings = [[(0,)]] + [[] for i in xrange(n)]
	for j in range(0, n):
		for i in strings[j]:
			strings[j + 1].append(i + (0,))
			strings[j + 1].append(i + (1,))
		# print strings[j + 1]
	return strings


def transition(d0, d1, state, bit):
	if (bit == 0):
		return d0[state]
	if (bit == 1):
		#print "d0: " + str(d0)
		#print "State: " + str(state)
		return d1[state]

def state(d0, d1, x, i):
	# the state after reading i bits
	if (i == 0):
		return 0
	if (i > 0):
		return transition(
			d0,
			d1,
			state(d0, d1, x, i - 1),
			x[i - 1]
		)

def endState(d0, d1, x):
	#return state(d0, d1, x, len(x))
	#or to be non-recursive:
	state = 0
	for i in range(0, len(x)):
		state = transition(d0, d1, state, x[i])
	return state

def diracDelta(a, b):
	if (a == b):
		return 1
	else:
		return 0

def checkWitnesses(d0, d1, n, q, m):
	M = [[ diracDelta(d0[i], j) + diracDelta(d1[i], j) for j in range(0,q)] for i in range(0,q)]
	Mpower = numpy.linalg.matrix_power(M, n)
	for x in itertools.product((0, 1), repeat=n):
		#print "Considering " + str(x)
		if not (x in foundStrings):
			qfinal = endState(d0, d1, x)
			numPaths = Mpower[0, qfinal]
			#print
			#print "qfinal is " + str(qfinal)
			if numPaths >0 and numPaths <= 2**m: #was: == 1:
					foundStrings[x] = True
					print "Number of paths: " + str(numPaths)
					print str(len(foundStrings)) + " / " + str(2**n)
					print "A(" + str(x) + ", m=" + str(m) + ") <="+str(q)
					print "d0: " + str(d0)
					print "d1: " + str(d1)
					print
				#print "qfinal: " + str(qfinal)
				#print "Matrix: " + str(M)

def checkWitnessesForPermutationConjecture(d0, d1, n, q, m):
	M = [[ diracDelta(d0[i], j) + diracDelta(d1[i], j) for j in range(0,q)] for i in range(0,q)]
	Mpower = numpy.linalg.matrix_power(M, n)
	for qfinal in range(0, q):
		numPaths = Mpower[0, qfinal]
		if numPaths >0 and numPaths <= 2**m: #was: == 1:
			print "Number of paths: " + str(numPaths)
			print "d0: " + str(d0)
			print "d1: " + str(d1)
			print
			#print "qfinal: " + str(qfinal)
			#print "Matrix: " + str(M)


def checkWitness(d0, d1, x, q):
	#print str(d1)
	#M is matrix 1[d0[i]=j]+1[d1[i]=j]
	#M = [[ 1[d0[i]=j]+1[d1[i]=j] for i in range(0,n)] for j in range(0,n)]
	qfinal = endState(d0, d1, x)
	M = [[ diracDelta(d0[i], j) + diracDelta(d1[i], j) for j in range(0,q)] for i in range(0,q)]
	numPaths = numpy.linalg.matrix_power(M, len(x))[0, qfinal]
	#print
	#print "qfinal is " + str(qfinal)
	if numPaths == 1:
		print "Eureka?"
		print "d0: " + str(d0)
		print "d1: " + str(d1)
		print "qfinal: " + str(qfinal)
		print "Matrix: " + str(M)
		print
		return True
	return False
	#print "sorry"

def DFAcomplexity(x):
	for q in range(2, qfixed+1):
		zeroThroughQMinus1 = [()]
		for i in range(0,q):
			zeroThroughQMinus1.append(zeroThroughQMinus1[i] + (i,))
		myStates = zeroThroughQMinus1[q]

		strings1 = itertools.product(myStates, repeat=q)
		for d0 in strings1:
			if slowishIncrease(d0):
				if not (d0[0] == 0 and d0[1] > 1):#since by finiteness, some states don't go right, and w.m.a. "1" is one of them
					#print "."
					print str(d0)
					strings2 = itertools.product(myStates, repeat=q)
					#print str(d0)
					for d1 in strings2:
						#if slowishIncrease(d1): NO! fast, but wrong...
						if checkWitness(d0, d1, x, q):
							return
		print "The DFA complexity of " + str(x) + " is strictly more than " + str(q)

def checkBijection(d):
	return (len(set(d)) == len(d))
	#for i in range(0, len(d)):
	#	found = False
	#	for j in range(0, len(d)):
	#		if d[j] == i:
	#			found = True
	#	if not found:
	#		return False
	#return True

def allDFAcomplexities(n, checkPermutations=False,m=0, startQ=1):
	for q in range(startQ, n+2):#was n+1
		#print "Considering q=" + str(q)
		myStates = tuple([i for i in range(0, q)])
		strings1 = itertools.product(myStates, repeat=q)
		for d0 in strings1:
			if len(d0)>1 and not (d0[0] == 0 and d0[1] > 1):#since by finiteness, some states don't go right, and w.m.a. "1" is one of them if 0->0. 2015-09-08
				if slowishIncrease(d0):
					if checkPermutations:
						if not checkBijection(d0):
							continue
				#more generally if a state has not been reached by previous states, we can assume it does not go right -- code that up too
					#print "."
					print str(d0)
					strings2 = itertools.product(myStates, repeat=q)
					#print str(d0)
					for d1 in strings2:
						if checkPermutations:
							if not checkBijection(d1):
								continue
						#if slowishIncrease(d1): NO! fast, but wrong...
						checkWitnesses(d0, d1, n, q, m)

def checkPermutationConjecture(n):
	q = n
	m = 0
	checkPermutations = True
	myStates = tuple([i for i in range(0, q)])
	#strings1 = itertools.product(myStates, repeat=q)
	for d0 in itertools.product(myStates, repeat=q):
		if len(d0)>1 and not (d0[0] == 0 and d0[1] > 1):#since by finiteness, some states don't go right, and w.m.a. "1" is one of them if 0->0. 2015-09-08
			if slowishIncrease(d0):
				if checkPermutations:
					if not checkBijection(d0):
						continue
			#more generally if a state has not been reached by previous states, we can assume it does not go right -- code that up too
				#print "."
				print str(d0)
				print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
				#strings2 = itertools.product(myStates, repeat=q)
				#print str(d0)
				old1 = tuple([0 for i in range(0, q)])
				for d1 in itertools.product(myStates, repeat=q):
					if d1[1] != old1[1] and d1[1] % 2 == 0:
						print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
					old1 = d1
					if checkPermutations:
						if not checkBijection(d1):
							continue
					#if slowishIncrease(d1): NO! fast, but wrong...
					checkWitnessesForPermutationConjecture(d0, d1, n, q, m)

def checkAllZerosPermutationConjecture(n):
	q = n
	m = 0
	checkPermutations = True
	myStates = tuple([i for i in range(0, q)])
	#strings1 = itertools.product(myStates, repeat=q)
	d0 = tuple((i for i in range(1, q)))
	d0 = d0 + (0,)
	#for d0 in itertools.product(myStates, repeat=q):
	#	if len(d0)>1 and not (d0[0] == 0 and d0[1] > 1):#since by finiteness, some states don't go right, and w.m.a. "1" is one of them if 0->0. 2015-09-08
	#		if slowishIncrease(d0):
	#			if checkPermutations:
	#				if not checkBijection(d0):
	#					continue
	#		#more generally if a state has not been reached by previous states, we can assume it does not go right -- code that up too
	#			#print "."
	print str(d0)
	print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	#strings2 = itertools.product(myStates, repeat=q)
	#print str(d0)
	old1 = tuple([0 for i in range(0, q)])
	for d1 in itertools.product(myStates, repeat=q):
		if d1[2] != old1[2]:# and d1[1] % 2 == 0:
			print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		old1 = d1
		if checkPermutations:
			if not checkBijection(d1):
				continue
		#if slowishIncrease(d1): NO! fast, but wrong...
		checkWitnessesForPermutationConjecture(d0, d1, n, q, m)

for n in range(10, 11):
	print "Checking all zeros permutation conjecture for n=" + str(n)
	checkAllZerosPermutationConjecture(n)
raise SystemExit

for n in range(10, 11):
	print "Checking permutation conjecture for n=" + str(n)
	checkPermutationConjecture(n)
raise SystemExit
#for l in range(0, 8):
l=9
foundStrings = {}
allDFAcomplexities(l,True,m=0, startQ=l)
raise SystemExit