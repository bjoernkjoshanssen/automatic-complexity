from dualStructureFunctionDataDeterministic import dataDet
from dualStructureFunctionData import dataPath
from dualStructureFunctionDataWordCounting import dataWord

def pValueFromCount(pValue, n):
	return str(round(pValue/float(2**(n-1)),2))

for i2 in range(0, len(dataWord)):
	for i1 in range(0, len(dataPath)):
		if dataWord[i2][0] == dataPath[i1][0]:
			myWord = dataPath[i1][0]
			if dataWord[i2][1] != dataPath[i1][1]:
				print str(
					[
                        j for j in range(0, len(dataWord[i2][1])) if (
                            dataWord[i2][1][j] != dataPath[i1][1][j]
                        )
                    ]
				) + "\t for " + str(myWord) + " with ",

				print (
                    str(dataWord[i2][1])
                    + " != " + str(dataPath[i1][1]),
                )
				for pair in dataPath:
					[first, second] = pair
					if myWord == first:
						h = second
						break
				pValues = [0 for i in range(0,len(myWord) + 1)]
				for pair in dataPath:
					[first, second] = pair
					if len(first) == len(myWord):
						for i in range(0, len(myWord) + 1):
							if second[i] <= h[i]:
								pValues[i] += 1
				minPValue = min(pValues)
				argMinPValue = pValues.index(minPValue)
				
				print pValues,
				print (
                    "\tPath-based p-value at m=" + str(argMinPValue)
                    + " was " + pValueFromCount(minPValue, len(myWord)),
                )

				#... and the word-based version:
				for pair in dataWord:
					[first, second] = pair
					if myWord == first:
						h = second
						break
				pValues = [0 for i in range(0,len(myWord) + 1)]
				for pair in dataWord:
					[first, second] = pair
					if len(first) == len(myWord):
						for i in range(0, len(myWord) + 1):
							if second[i] <= h[i]:
								pValues[i] += 1
				print "\t" + str(pValues),
				minPValueWord = min(pValues)
				argMinPValueWordBased = pValues.index(minPValueWord)
				print (
                    "Word-based p-value at m=" + str(argMinPValueWordBased)
                    + " was "
                    + str(round(minPValueWord/float(2**(len(myWord)-1)),2)),
                )
				if argMinPValueWordBased != argMinPValue:
					print (
                        "Different explanations, p-values "
                        + str(round(minPValueWord/float(2**(len(myWord)-1)),2))
                        + " and "
                        + str(round(minPValue/float(2**(len(myWord)-1)),2))
                    )
				else:
					print
print
print
print "Deterministic versus path-based nondeterministic"
print
print

# and now to compare deterministic and path-based nondeterministic:

for i2 in range(0, len(dataDet)):
	for i1 in range(0, len(dataPath)):
		if dataDet[i2][0] == dataPath[i1][0]:
			myWord = dataPath[i1][0]
			if dataDet[i2][1] != dataPath[i1][1]:
				print str(
					[
                        j for j in range(
                            0, len(
                                dataDet[i2][1]
                            )
                        ) if (
                            dataDet[i2][1][j] != dataPath[i1][1][j]
                        )
                    ]
				) + "\t for " + str(myWord) + " with ",

				print (
                    str(dataDet[i2][1])
                    + " != " + str(dataPath[i1][1]),
                )
				for pair in dataPath:
					[first, second] = pair
					if myWord == first:
						h = second
						break
				pValues = [0 for i in range(0,len(myWord) + 1)]
				for pair in dataPath:
					[first, second] = pair
					if len(first) == len(myWord):
						for i in range(0, len(myWord) + 1):
							if second[i] <= h[i]:
								pValues[i] += 1
				print str(pValues)
				minPValue = min(pValues)
				argMinPValue = pValues.index(minPValue)
				print (
                    "Path-based p-value at m=" + str(argMinPValue)
                    + " was " + str(round(minPValue/float(2**(len(myWord)-1)),2)),
                )

				#... and the deterministic version:
				for pair in dataDet:
					[first, second] = pair
					if myWord == first:
						h = second
						break
				pValues = [0 for i in range(0,len(myWord) + 1)]
				for pair in dataDet:
					[first, second] = pair
					if len(first) == len(myWord):
						for i in range(0, len(myWord) + 1):
							if second[i] <= h[i]:
								pValues[i] += 1
				print str(pValues)
				minPValueDet = min(pValues)
				argMinPValueDeterministic = pValues.index(minPValueDet)
				print (
                    "Deterministic p-value at m=" + str(argMinPValueDeterministic)
                    + " was "
                    + str(round(minPValueDet/float(2**(len(myWord)-1)),2)),
                )
				if argMinPValueDeterministic != argMinPValue:
					print (
                        "Different explanations, p-values "
                        + str(round(minPValueDet/float(2**(len(myWord)-1)),2))
                        + " and "
                        + str(round(minPValue/float(2**(len(myWord)-1)),2))
                    )
				else:
					print
