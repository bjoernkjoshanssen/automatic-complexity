#! /usr/bin/env python
# from __future__ import print_function
import numpy
import time
import sys
import math
import pickle
import random
from automatoncomplexity import SimpleStrings
"""
Functions in this file generate reports about automaton complexity
using the class SimpleStrings.

Functions and their dependencies:
	checkSimplicity -> recursiveCheckSimplicity
	getInverseStructureFunction
	generateAllBinaryStrings	
"""
import itertools

from automatoncomplexity import piOne

def flexibleStructureFunction(
	xseq,
	b=2,
	printStateSequences=False,
	printStats=False,
	checkDeterminism=False,
	wordBased=False,
	mMax=-1,
	verbose=False,
	endStateIsStartState = False
): # set mMax=0 if only want complexity, not structure function.
	# flexible, meaning either wordBased or path-based.
	# we don't want checkDeterminism and wordBased at the same time.
	#print "flexibleStr.. says endSt.. is " + str(endStateIsStartState)
	n = len(xseq)
	if mMax == -1:
		mMax = len(xseq)
	aboveMyStructureFunction = set()
	
	#for q in range(1, n/2 + 1 + 1): # BUT WHAT BOUND TO USE IN THE DETERMINISTIC CASE???
	#so instead do:
	q = 0
	#allDone = False
	while True: #not allDone:
		q += 1

		m = 0
		#print "q=" + str(q)

		#find the path-based structure function first, then the guesses for word-based start there and move left.
		while not checkSimplicity(xseq, q, b, m,
			checkDeterminism=checkDeterminism,
			printStateSequences=printStateSequences,
			printStats=printStats,
			verbose=verbose,
			endStateIsStartState=endStateIsStartState
		) and m <= mMax:
			#if q > 3: # to limit printing
			#	print "q=" + str(q) + ", m=" + str(m) + " not OK path-based."
			m += 1


		if wordBased:
			#mUpperBound = m
			m -= 1
			#print str(m)
			while wordBasedStructureFunctionPoint(
				xseq, n=n, q=q, b=b, m=m,
				printStateSequences=printStateSequences,
				printStats=printStats,
				verbose=verbose,
				endStateIsStartState=endStateIsStartState
			) and m >= 0:
				#print "q=" + str(q) + ", m=" + str(m) + " OK word-based."
				m -= 1
		#print "Got m=" + str(m)
			m += 1
		#print "for q=" + str(q) + ", got m=" + str(m)
		for mm in range(m, mMax + 1):
			aboveMyStructureFunction.add((mm, q))
		if m == 0:
			break #in lieu of letting allDone = True
	mygraph = []
	for m in range(0, mMax + 1):

		overgraph = [qq for qq in range(1, q+1) if (m,qq) in aboveMyStructureFunction]
		# WAS: overgraph = [qq for qq in range(1, n/2+1+1) if (m,qq) in aboveMyStructureFunction] BUT IN DETERMINISTIC CASE THAT'S NO GOOD

		if overgraph != []:
			mygraph += [min(overgraph)]
	return mygraph


def displayStructureFunction(
	xseq,
	printStateSequences = False,
	printStructureFunction = True,
	printStats = False,
	checkDeterminism = False,
	wordBased = True,
	mMax = -1,
	verbose = False,
	endStateIsStartState = False,
	b=2
):
	#print "displayStructureFunction says endSt.. is " + str(endStateIsStartState)
	n = len(xseq)
	#if not verbose:
	print "["
	print "\t" + str(xseq) + ","
	if printStateSequences:
		print "\t["
	#else:
	#	print "\t\t(",

	if printStructureFunction:
		print "\t" + str(flexibleStructureFunction(xseq, b=b,
			printStateSequences=printStateSequences,
			printStats=printStats,
			checkDeterminism=checkDeterminism,
			wordBased=wordBased,
			mMax=mMax,
			verbose=verbose,
			endStateIsStartState=endStateIsStartState
		))
	else:
		flexibleStructureFunction(
			xseq, b=b,
			printStateSequences=printStateSequences,
			printStats=printStats,
			checkDeterminism=checkDeterminism,
			wordBased=wordBased,
			mMax=mMax,
			verbose=verbose,
			endStateIsStartState=endStateIsStartState
		)
	if printStateSequences:
		print "\t]"
	#else:
	#	print "),"
	print "],"

def checkSimplicity(
		xseq,
		q,
		b=2,
		m=0,
		checkDeterminism=False,
		verbose=False,
		printStateSequences=False,
		printStats=False,
		endStateIsStartState = False
	):# m is 0 by default
	#print "checkSim... says endSt.. is " + str(endStateIsStartState)
	n = len(xseq)
	# q = n/2 # or n/2 + 1 or whatever you want
	# caution: other functions may have assumed that
	# q was defined here and not an argument to checkSimplicity
	# b = max(xseq) + 1 # This may be a problem. For structure functions we instead want to specify b.
	useFiles = False
	# print "running simplestrings for ",q," state and string length ",n,
	# print "and ",b,"-size alphabet and usefiles=",useFiles,"and m=",m,")"
	# print "string ",xseq
	s = SimpleStrings(q, n, b, useFiles, m)
	#print "m=" + str(m)
	s.complexity(
		xseq,
		checkDeterminism,
		verbose,
		printStateSequences=printStateSequences,
		printStats=printStats,
		endStateIsStartState=endStateIsStartState
	)
	# print s.numSimple()
	return s.numSimple()

def getInverseStructureFunction(myseq, checkDeterminism=False, verbose=False, wordBased=False): # if not word-based then path-based.
	myStructureFunction = set()
	myStructureDisplay = dict()
	if verbose:
		print
		print str(myseq) + " = string being considered"
	else:
		print "["
		print "\t" + str(myseq) + ","
		print "\t["
	allDone = False
	for myq in range(1, len(myseq)/2 + 2):
		if allDone:
			if verbose:
				print "\t0",
			# break
		else:
			"""
			note:
			start with myq==2, since for myq==1,
			the program does not find
			the automaton that accepts everything, with one state.
			Because it only looks for automata generated as
			sequences-of-states with no allowance for multiple edges
			in the same direction between the same two vertices.
			Not sure whether this is an equivalent definition?
			In the meantime, make that the official definition?
			It does generalize the h_x(0) case b/c of uniqueness
			... have improved it now, and it seems to have made no difference.
			the case m=n is still wrong though
			"""
			# print
			for m in range(0,len(myseq) + 1):
				if wordBased:
					# need to put:
					#	print "\t\t" + str(seq) + ","
					# every time another suitable seq is found.
					pass
				if not wordBased:
					if checkSimplicity(myseq, myq, m, checkDeterminism): # if h_{myseq}(m)\le myq
						if (myq != 1):
							if verbose:
								print "h_x^(-1)(" + str(myq) + ")=" + str(m)
								print str(myq) + " " + str("X"*m)
							myStructureFunction.add((myq, m))
							myStructureDisplay[myq] = str(myq) + " " + str(" 0"*m) + " 1"
							# print "\t",m,
						if m==0:
							allDone = True
						break # exit the for loop
					# else:
					#	print "h_x(" + str(m) + ")>=" + str(myq + 1)
					#.............................)}:
	if verbose:
		print str(myStructureFunction)
		for k in range(int(len(myseq)/2+1), 2-1, -1): # myStructureDisplay:
			if k in myStructureDisplay:
				print myStructureDisplay[k]
		print "1 " + str(" 0"*(len(myseq))) + " 1"
		print "   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5"
		print "                      10  12  14"
	else:
		print "\t]"
		print "],"

def generateRandomString(n):
	myString = []
	for i in range(0, n):
		myString.append(random.randint(0,1),)
	return tuple(myString)

def stringify(seq):
	printableString = ""
	for bit in range(0, len(seq)):
		printableString += format(seq[bit], 'x')
	return printableString

def complexityFacts():
	theLength = 33
	theFile = open('length' + str(theLength) + '.json', 'w')
	# print "["
	theFile.write("[\n")
	for n in range(theLength, theLength + 1):
		# q = n/2 + 1
		q = 11
		leq = [0]*(q + 1)
		prob = [0]*(q + 1)
		occurrences = [0]*(q + 1)
		leq[0] = SimpleStrings(0, n)
		leq[0].allComplexityBounds()
		if n % 2 == 0:
			s = ""
		else:
			s = "\t\t\t\t\t\t"
		print ""
		print s,"Report for string length n =",n," and q=",q
		print s,"-------------------------------"
		for i in range(0, q + 1):
			start = time.clock()
			leq[i] = SimpleStrings(i, n)
			leq[i].allComplexityBounds()
			for seq in leq[i].matches:
				if not (seq in leq[i-1].matches):
					theFile.write(jsonFormatted(seq, i))
			# print s,"Finding SimpleStrings(",i,",",n,") took", str(time.clock() - start)
			print s,leq[i].numInterestingSequences," interesting sequences for n=",leq[i].n,"and q=",leq[i].q
			# print "Namely",leq[i].interestingSequences
			leq[i].saveUniquelyAcceptingStateSeqs
			if i > 0:
				occurrences[i] = leq[i].numSimple() - leq[i - 1].numSimple()
			else:
				occurrences[i] = leq[i].numSimple()
			prob[i] = occurrences[i]/float(2**n)
			# print s,"Complexity", i, "occurs", str(prob[i])," of the time, a total of ",occurrences[i],"times"
		# print s,"Complexity", q + 1, "occurs", str(1 - sum(prob)),
		# print " of the time, a total of ",2**n- sum(occurrences),"times, i.e. the rest of the time"
		e = 0
		for i in range(0, q + 1):
			e += prob[i] * i
		print s,"Expected complexity deficiency:", q - e
		# chisquare = 0
		# for i in range(0, q):
		#	p = prob[q - i]
		#	pe = poisson(q - e, i)
		#	chisquarecontribution = (p - pe)**2/float(pe)*(2**n)
		#	chisquare += chisquarecontribution
		#	print s,"Deficiency", i, "occurs",
		#	print (leq[q - i].numSimple() - leq[q - i - 1].numSimple())," times, predicted: ",
		#	print (poisson(q - e, i)*(2**n)), "chi^2 contribution:",chisquarecontribution
		# print s,"Chi^2 statistic:",chisquare
		# print s,"Degrees of freedom k-2 (since lambda estimated) where k is n/2+1:",q -2 
	# print "]"
	theFile.write("]")


def cumulativeFractions():
	maxq = 12
	numSimpleArray = [0] * (2*maxq + 1)
	numSimpleArray[0] = 0# the empty string is not simple
	print "Cumulative fraction of simple strings after lengths:"
	for q in range(0, maxq):
		start = time.clock()
		s = SimpleStrings(q, 2 * q)
		s.allComplexityBounds()
		numSimpleArray[s.n] = s.numSimple()
		print s.n, ":",
		print "%.2f" % 	(sum(numSimpleArray[0:]) / float(2**(s.n+1) - 1)), '(seconds:', str(time.clock() - start), ")"

		start = time.clock()
		s = SimpleStrings(q, 2 * q + 1)
		s.allComplexityBounds()
		numSimpleArray[s.n] = s.numSimple()
		print "\t\t\t\t\t", s.n, ":",
		print "%.2f" % 	(sum(numSimpleArray[0:]) / float(2**(s.n+1) - 1)), '(seconds:', str(time.clock() - start), ")"


def convergence():
	print "It appears the fraction of complex strings may approach 1/2:"
	numSimple = (
	#	(0, 0, 0),
		(1, 2, 2),
		(2, 8, 8),
		(3, 34, 30),
		(4, 158, 106),
		(5, 680, 650),
		(6, 2458, 2418), 
		(7, 11268, 9750),
		(8, 43060, 44944),
		(9, 172578, 173038),
		(10, 672866, 635482),
		(11, 2655140, 2701374),
		(12, 9962434, 9522756)
	)
	for (n,e,o) in numSimple:
		even = e/float(2**(2*n))
		odd = o/float(2**(2*n+1))
		approx = (2 - (even+odd))/2
		print "%.2f" % approx,
		b = int((approx-1)*100)
		for c in range(0, 26):
			if c <= b:
				print "x",
			else:
				print " ",
		print "%.2f" % even,
		print "%.2f" % odd


def recursiveCheckSimplicity(xseq):
	if len(xseq) == 19:
		return
	if checkSimplicity(xseq) == 1:
		return
	else:
		print xseq, " is hereditarily complex",
		if len(xseq) == 18:
			print "!"
		else:
			print ""
		recursiveCheckSimplicity(xseq + (0,))
		recursiveCheckSimplicity(xseq + (1,))


def structureFunctionPoint(myStrings, mStart, mEnd, qStart, qEnd, shiftStart, shiftEnd, checkDeterminism):
	for mm in range(mStart, mEnd + 1):
		for myq in range(qStart, qEnd + 1): #10 to 15
			print
			print "Testing q=" + str(myq)
			for baseSeq in myStrings:
				xseq = baseSeq
				print
				print "Testing " + str(baseSeq) + " cyclically"
				print
				for shift in range(shiftStart, shiftEnd + 1): #(0,31), i.e. 0 to 30, as 31 is equivalent to 0
					t0 = time.time()
					print
					print "Testing",
					print str(xseq),
					print "for complexity at most " + str(myq) + " with mm=" + str(mm)
					#print len(xseq)
					s = SimpleStrings(
						q = myq, n = len(xseq), b = 2, useFiles = False, m = mm, isItTimeToReturn=False, returnWhenFound=True
					)
					print "..."
					s.complexity(xseq, checkDeterminism=checkDeterminism)
					t1 = time.time()
					total = t1-t0
					print "(" + str(round(total/60,2)) + " minutes)"
					xseq = (xseq[len(xseq) - 1],) + xseq[0:len(xseq) - 1]


def slowincrease(seq, q):
	"""
	empirical fact: witnessing state sequences rarely, but not never,
	need to jump two ahead
	"""
	currmax = -1
	for j in xrange(0, len(seq)):
		if seq[j] > currmax + 1:
			return False
		if seq[j] == currmax + 1:
			currmax += 1
		if currmax == q - 1:
			return True
	return True


def issubtuple(r, l): # https://geert.vanderkelen.org/2010/find-out-if-every-element-of-a-list-is-part-of-another-with-python/
	#myBool = (False not in [ e in l for e in r ])
	myBool = (set(r) <= set(l))
	return myBool

def superfluous(seq, xseq, n):
	for j1 in xrange(0, n):
		for j2 in xrange(j1+1,n):
			if seq[j1] == seq[j2] and seq[j1 + 1] == seq[j2 + 1] and xseq[j1] == xseq[j2]:
				return True #two edges between the same pair of states
			for j3 in xrange(j2+1,n+1):
				if seq[j1] == seq[j1 + 1] and seq[j2] == seq[j3] and seq[j1] != seq[j2]:
					return True #loops of length one at different states
					# any time-disjoint loops (at different states) of lengths say 2,3,3,5 where there is more than one way to obtain the sum 2+3+3+5 from those {2,3,5}
					# is "superfluous" in this sense. proof? it works if one of the lengths can be totally eliminated.
					# say in 1,1,1,5 we can make it 1,1,1,1,1,1,1,1 (unique solution in {1}).
					# And in 2,3,3,5 we can make it 3,5,5 (unique solution in {3,5}). But can that always be done?
					# Can we always eliminate one of the "coins" so as to get a unique solution?
					# No, consider 3+3+3+3+3+3+5 = 23; we can make it 3+5+5+5+5 but we cannot eliminate 3 nor 5.
					# And if we can't eliminate a coin, whatever we do won't fix the problem, so then who's to say that the combinations is superfluous/useless?
					# A^{path} is but A^{word} isn't
					#
					# how many state sequences are not superfluous in this sense?
					# 0
					# 0,0
					# 0,1
					# 0,0,0
					# 0,0,1
					# 0,1,0
					# 0,1,1
					# 0,1,2
					# 0,0,0,0
					# 0,0,0,1
					# 0,0,1,0
					# 0,0,1,1 superfluous
					# 0,0,1,2
					# 0,1,0,0 superfluous
					# 0,1,0,1
					# 0,1,0,2
					# 0,1,1,0
					# 0,1,1,1
					# 0,1,1,2
					# 0,1,2,0
					# 0,1,2,1
					# 0,1,2,2
					# 0,1,2,3
					# 0,0,0,0,0
					# 0,0,0,0,1
					# 0,0,0,1,0 superfluous... well, it's the same base state 0 but not the same sequence of states in the loop (00 or 000 vs 010).
					# 0,0,0,1,1 superfluous
					# 0,0,0,1,2
					# 0,0,1,0,0 superfluous
					
		for j2 in xrange(0, j1): # the loop of length one could also come before...
			for j3 in xrange(j2 + 1, j1):
				if seq[j1] == seq[j1 + 1] and seq[j2] == seq[j3] and seq[j1] != seq[j2]:
					return True #loops of length one at different states
	return False

def possibleStates(yseq, n, newttt, q, seq):
	oldPossibleStates = set()
	oldPossibleStates.add(seq[0]) # seq[0] may not equal 0, if we are not using slow state sequences

	for j in xrange(0, n):#n==len(yseq)
		newPossibleStates = set(
			[
				q1 for q1 in xrange(0,q) for q0 in oldPossibleStates if (q0,q1,yseq[j]) in newttt
			]
		)
		oldPossibleStates = newPossibleStates
	return newPossibleStates


def checkStringWithinPathsVsWordsConjecture(xseq, n, q, b=2, m=0):
	# checkString(xseq, zeroThroughQMinus1, s, n)
	if xseq[0] != 0: # no need to consider strings starting with 1
		return False
	#print
	#print "Word being considered: " + str(xseq)
	bingo = False
	#noSupersets = set() # this is modified by various seq's. It's not actually important for counting simple strings
	zeroThroughQMinus1 = tuple([i for i in range(0, q)])
	#allSeqs = itertools.product(zeroThroughQMinus1, repeat=n+1)
	time0 = time.time()
	minEdges = n #the highest number of edges we could possibly use
	multiPathsFound = False
	#for seq in allSeqs:
	for seq in itertools.product(zeroThroughQMinus1, repeat=n+1):
		if not slowincrease(seq, q):
			continue #not break!
		if m == 0:
			if superfluous(seq, xseq, n):
				continue
		#print seq
		time1 = time.time()
		if time1 - time0 > 6:
			print time.asctime( time.localtime(time.time()) )
			print "State sequence being considered: " + str(seq)
			time0 = time1
		finalState = seq[n] # was: finalState = seq[len(seq) - 1]
		newttt = tuple(set([(seq[j], seq[j+1], xseq[j]) for j in xrange(0, n)])) # tuple to make it hashable; set to make it nonrepetitive. note n==len(xseq)

		numAccepted = 0
		zeroThroughBMinus1 = tuple([i for i in range(0, b)])
		strings = itertools.product(zeroThroughBMinus1, repeat=n)
		acceptedStrings = set()
		for yseq in strings: #string[n]:
			oldPossibleStates = set()
			oldPossibleStates.add(seq[0]) # seq[0] may not equal 0, if we are not using slow state sequences

			for j in xrange(0, n):#n==len(yseq) # MAKE THIS A FUNCTION TO MONITOR TIME SPENT IN IT
				newPossibleStates = set([q1 for q1 in xrange(0,q) for q0 in oldPossibleStates if (q0,q1,yseq[j]) in newttt])
				oldPossibleStates = newPossibleStates
			if finalState in newPossibleStates:
				#print "string " + str(yseq) + " accepted."
				acceptedStrings.add(yseq)
				numAccepted += 1
				if numAccepted > b**m:
					break # out of the for yseq loop
				#setOfAccepted.add(yseq)
				lastAccepted = seq
				lastAcceptedYseq = yseq
		#print str(numAccepted) + " strings accepted, namely " + str(acceptedStrings)
		#print
		if numAccepted <= b**m:
			# Initialize transitionTable (comment this out if don't care about # of paths):
			#transitionTable = numpy.zeros(shape=(q, q))
			print "h_\{" + str(xseq) + "\}^\{*word\}(m=" + str(m) + ")\le q=" + str(q)
			transitionTable = [
				[
					sum(
						int(
							(p,pp,bb) in newttt
						) for bb in range(0, b)
					) for pp in xrange(0,q)
				] for p in xrange(0,q)
			]
			numPaths = numpy.linalg.matrix_power(transitionTable, n)[seq[0], finalState] # seq[0]!=0 is possible if not using slow state sequences

			#print "BINGO for seq " + str(lastAccepted)
			if numPaths > b**m and not multiPathsFound:
				multiPathsFound = True
				print "multiPathsFound with " + str(len(newttt)) + " edges."
			if numPaths > b**m and len(newttt) <= minEdges and multiPathsFound: #temporarily if
				print str(numAccepted) + " many words, but more paths for state sequence " + str(seq) + ", edges: " + str(len(newttt))
				minEdges = len(newttt)
			if numPaths <= b**m and len(newttt) < minEdges and multiPathsFound:
				print str(numAccepted) + " many words, " + str(numPaths) + " paths for state sequence " + str(seq) + ", edges: " + str(len(newttt))
				minEdges = len(newttt)
			if len(newttt) < minEdges:
				minEdges = len(newttt)
				#print "minEdges: " + str(minEdges)
			bingo = True
			return bingo # if want to save time
	return bingo

def checkOfThePathsVsStringsConjecture(n, q, b=2):
	#q = int(math.floor(n/2)) # target complexity (1 less than path-based complexity!)
	m = 0
	numberSimple = 0
	zeroThroughBMinus1 = tuple([i for i in range(0, b)])
	#strings = itertools.product(zeroThroughBMinus1, repeat=n)
	#allSeqs = [seq for seq in allSeqs if slowincrease(seq, q)] #this is too slow to do for each
	#for xseq in strings: #strings[n]: # note as soon as you're done iterating over this, it's gone!
	for xseq in itertools.product(zeroThroughBMinus1, repeat=n):
		#print str(xseq)
		if not slowincrease(xseq, len(xseq)):
			continue
		bingo = checkStringWithinPathsVsWordsConjecture(xseq, n, q, b)
		if bingo:
			print "A^{word} small for xseq=" + str(xseq)
			numberSimple += 1
			if checkSimplicity(xseq, q, 0) != 1:
				print "Conjecture seems to have been refuted."
				raise SystemExit
			print "Conjecture still looks OK."
	print "numberSimple = " + str(numberSimple)
