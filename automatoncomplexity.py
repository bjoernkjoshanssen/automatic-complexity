import numpy
import time
import sys
import math
import pickle

class SimpleStrings:
	def __init__(self, q, n, b=2, useFiles=True, m=0, isItTimeToReturn=False, returnWhenFound=False, checkResetDeterminism=False, approxLength=False):#caution: may need returnWhenFound=False when counting 
		"""
		Input variables:
			q       	int    	number of states
			n       	int    	length of string
			b       	int    	alphabet size for string; default value is 2
			useFiles	boolean	try to load local files
								where computation results may have been saved?
			m			int		look for automata that accept
								at most 2^m strings; default is 0
		Each of these become a property of the class:
		"""
		self.q = q
		self.n = n
		self.b = b
		self.m = m
		self.isItTimeToReturn = isItTimeToReturn
		self.returnWhenFound = returnWhenFound
		self.checkResetDeterminism = checkResetDeterminism
		self.edges = [0]*(self.n + 1) # since the number of edges visited is upper bounded by the length of the input string
		self.edges = numpy.array(self.edges)
		self.useFiles = useFiles
		self.bTom = self.b**m 	# better not recalculate this over and over...
		self.firstTime = time.time()
		self.lastTime = time.time()
		self.approxLength = approxLength
		"""
		Python data types to be used
		and how to give an 'empty' instance of each:
			dict     	dict()
			list     	[]
			set      	set()
			frozenset	frozenset()
			int      	0
		We initialize some variables:
			matches	catalog of which strings are simple
		"""
		self.emptyttt = frozenset()
		self.matches = set()
		self.matchesPairs = set()
		self.witness = dict()
		self.numWitnesses = dict()
		self.numInterestingSequences = 0
		self.interestingSequences = set()
		"""
		helperSeqs:
			to compute information for (self.n, self.q) we rely on
			(n, q) != (self.n, self.q) with
				0\le n\le self.n and
				1\le q\le self.q
			helperSeqs[n][q] is a set (which may be later cast to list)
			to enforce that there are no repeated entries.
			Its len will be the no. of uniqely accepting state sequences
		"""
		self.helperSeqs = [[0]] * (self.n + 1)
		for n in range(0, self.n + 1):
			self.helperSeqs[n] = [set()] * (self.q + 1)
		"""
		Put more values into helperSeqs:
		"""
		if self.useFiles:
			self.loadUniquelyAcceptingStateSeqs()
		"""
		End of __init__
		"""

	def loadUniquelyAcceptingStateSeqs(self):
		"""
		first load information for
			n < self.n and
			q <= min(n + 1, self.q)
			(presumably self.q <= self.n + 1
			as more states than that would be superfluous)
		then
			n = self.n and
			q <= min(n + 1, self.q - 1)
		"""
		for n in range(0, self.n):
			for q in range(1, n + 2):
				if q <= self.q:
					try:
						self.helperSeqs[n][q] = pickle.load(
							open(
								"data/n = " + str(n) +
								", q = " + str(q) + ".txt",
								"rb"
							)
						)
					except:
						print ""
						print "Failed to load the file:"
						print "n = " + str(n) + ", q = " + str(q) + ".txt"
		for n in range(self.n, self.n + 1):
			for q in range(1, n + 2):
				if q < self.q:
					try:
						self.helperSeqs[n][q] = pickle.load(
							open(
								"data/n = " + str(n) +
								", q = " + str(q) + ".txt",
								"rb"
							)
						)
					except:
						print ""
						print "Failed to load the file:"
						print "n = " + str(n) + ", q = " + str(q) + ".txt"
		"""
		End of loadUniquelyAcceptingStateSeqs
		"""

	def complexity(
		self,
		xseq,
		checkDeterminism=False,
		verbose=False,
		printStateSequences=False,
		printStats=False,
		endStateIsStartState=False
	):
		#print "complexity() says endSt... is " +str(endStateIsStartState)
		"""
		Computes the nondeterministic automatic complexity of
			xseq
		by calling
			complexityBounds
		"""
		"""
		tt
			dtype=numpy.uint8 gives a bug since 257==1.
			numpy requires tt to be a square array, so not a set();
			but this is slow.
		usedAllStates
			True, unless we have reason to think otherwise
		"""
		tt = numpy.zeros(shape=(self.q, self.q), dtype=numpy.uint32)
		usedAllStates = True
		#print str(self.returnWhenFound)
		self.complexityBounds(
			(),
			tt,
			usedAllStates,
			xseq,
			checkDeterminism,
			verbose,
			printStateSequences=printStateSequences,
			printStats=printStats,
			endStateIsStartState=endStateIsStartState
		)

	def allComplexityBounds(self, checkDeterminism):
		tt = numpy.zeros(shape=(self.q, self.q))
		# ^^ dtype=numpy.uint8 gives a bug since 257==1.
		# numpy requires tt to be a square array, so not a set();
		# but this is slow.
		usedAllStates = (self.q == 1)
		# ^^ so usually useAllStates = False initially
		self.complexityBounds((), tt, usedAllStates, (), checkDeterminism) #self, seq, tt, usedAllStates, xseq, checkDeterminism=False
		self.saveUniquelyAcceptingStateSeqs()

	def slowincrease(self, seq):
		"""
		empirical fact: witnessing state sequences rarely, but not never,
		need to jump two ahead
		"""
		currmax = -1
		for j in xrange(0, len(seq)):
			if seq[j] > currmax + 1:
				return False
			if seq[j] == currmax + 1:
				currmax += 1
			if currmax == self.q - 1:
				return True
		return True

	def checkMatching(self, xseq, seq, ttt, checkDeterminism):
		newttt = set(ttt)
		j = len(xseq) - 1			# last input to xseq, if any
		if j >= 0:					# then update ttt and test matchingString
			p, pp = seq[j], seq[j + 1]
			edgeUsed = False
			for bb in range(0, self.b):
				if (p, pp, bb) in ttt:
					edgeUsed = True
					if bb != xseq[j]:
						# i.e., x[j] does not match previous value x[k]
						return  # matchingString failed
					break
			if not edgeUsed:
				newttt.add((p, pp, xseq[j]))
			if checkDeterminism:
				#deterministic = True
				#print "check for determinism"
				for p in range(0, self.q):
					for pp in range(0, self.q):
						for ppp in range(0, self.q):
							for b in range(0, self.b):
								if (p, pp, b) in newttt and (p, ppp, b) in newttt and pp != ppp:
									#deterministic = False
									return #matching failed
									#print str(newttt) + " is not deterministic."
				#end of check for determinism
		if len(xseq) < self.n:				# then spawn subprocesses
			for j in range(0, self.b):
				self.checkMatching(xseq + (j,), seq, newttt, checkDeterminism)
			return

		#print xseq," is simple as witnessed by",seq
		if not xseq in self.matches:
			print "matches so far: " + str(len(self.matches) + 1) + " including " + str(xseq)
		self.matches.add(xseq)  # since matchingString did not fail above.
		#if len(self.matches) % 100 == 0:
		#	print "matches so far: " + str(len(self.matches))
		self.matchesPairs.add((str(ttt), xseq))
		# 2015-06-26 IF WE DO SELF.MATCHESPAIRS.ADD(TTT,XSEQ) WE CAN FIND # OF ACCEPTED STRINGS BY AUTOMATON TTT
		self.witness[xseq] = seq
		if xseq not in self.numWitnesses:
			self.numWitnesses[xseq] = 1
		else:
			self.numWitnesses[xseq] += 1

	def matchesExtending(self, xseq, seq, ttt):
		"""
		Returns the set of all matches extending xseq, which is either:
			set() (if matching fails), or
			the union of the self.b many children's returns
			(if matching succeeds, and length is < max), or
			singleton of xseq (if matching succeeds, and length is max)
		Does not modify self.matches
		"""
		newttt = set(ttt)
		j = len(xseq) - 1			# last input to xseq, if any
		if j >= 0:					# then update ttt and test matchingString
			p, pp = seq[j], seq[j + 1]
			# ^^ MODIFY IN CASE SEQ IS SHORTER THAN SELF.N+1
			edgeUsed = False
			for bb in range(0, self.b):
				if (p, pp, bb) in ttt:
					edgeUsed = True
					if bb != xseq[j]:
						# i.e., x[j] does not match previous value x[k]
						return set()  # matchingString failed
					break
			if not edgeUsed:
				newttt.add((p, pp, xseq[j]))
		if len(xseq) < self.n - 1:
			# then spawn subprocesses WAS SELF.N; TEMP FIX
			myset = set()
			for j in range(0, self.b):
				myset = myset.union(
					self.matchesExtending(xseq + (j,), seq, newttt)
				)
			return myset
		# print xseq," is simple as witnessed by",seq
		myset = set()
		myset.add(xseq)
		return myset  # since matchingString did not fail above.

	def numSimple(self):
		return len(self.matches)

	def automatonFromStateSequence(self, seq):
		automaton = set()
		for i in range(0, len(seq) - 1):
			automaton.add(
				(seq[i], seq[i+1])
			)
		frozenAutomaton = frozenset(automaton)
		return frozenAutomaton

	def numInterestingAutomata(self):
		myAutomata = set()
		allMatches = set()
		setOfSequences = self.helperSeqs[self.n - 1][self.q - 1]
		sequences = list(setOfSequences)
		savedMatches = [set()] * len(sequences)
		for i in range(0, len(sequences)):
			savedMatches[i] = self.matchesExtending(
				(), sequences[i], self.emptyttt
			)
			allMatches = allMatches.union(savedMatches[i])
		numNeeded = 0
		NmodIsom = 0
		print """
<table class="table-01">
	<tr>
		<th>
			numNeeded
		</th>
		<th>
			State sequence
		</th>
		<th>
			oset
		</th>
		<th>
			seqDual
		</th>
		<th>
			New isomorphism class?
		</th>
		<th>
			Index of isomorph
		</th>
	</tr>
		"""
		for i1 in range(0, len(sequences)):
			seq1 = sequences[i1]
			automaton = self.automatonFromStateSequence(seq1)
			myAutomata.add(automaton)

			theMatches = savedMatches[i1]
			# allMatches = allMatches.union(theMatches)

			otherMatches = set()
			for i2 in range(0, len(sequences)):
				if i2 != i1:
					otherMatches = otherMatches.union(savedMatches[i2])

			if len(otherMatches) != len(allMatches):
				answer = True
				# "Yes, since " + str(len(otherMatches))
				# + "&lt;" + str(len(allMatches))
				# answer += self.gviz(seq1)
				numNeeded += 1
			else:
				answer = False  # "No"

			alist = seq1[::-1]
			mmap = {}  # implements hashed lookup
			oset = []  # storage for set
			for item in alist:
				# Save unique items in input order
				if item not in mmap:
					mmap[item] = 1
					oset.append(item)
			seqDual = [0] * len(seq1)
			seqBackwards = [0] * len(seq1)
			for jj in range(0, len(seq1)):
				seqBackwards[jj] = seq1[len(seq1) - 1 - jj]
			for jj in range(0, len(seq1)):
				seqDual[jj] = oset.index(seqBackwards[jj])
			newIsomClass = (tuple(seqDual) not in sequences[0:i1])
			if newIsomClass:
				indexOfIsomorph = "N/A"
			else:
				indexOfIsomorph = ""  # str(sequences.index(tuple(seqdual)))
			if answer:
				print """	<tr>
		<td>
			%s
		</td>
		<td>
			%s
		</td>
		<td>
			%s
		</td>
		<td>
			%s
		</td>
		<td>
			%s
		</td>
		<td>
			%s
		</td>
	</tr>""" % (
					numNeeded, seq1, oset, tuple(seqDual), newIsomClass,
					indexOfIsomorph
				)
			if answer and newIsomClass:
				NmodIsom += 1
		print "</table>"
		print "Total", numNeeded, " (needed) automata in smallest covers"
		print "Number of nonisomorphic needed automata:", NmodIsom
		"""
		Lemma.
			The min. size of covering set
			is bounded below by no. of binary digits in numSimple.
		Proof.
			If not, take two equal powers of two in the sum
			and combine to reduce the total number.
		"""
		return len(myAutomata)

	def checkMatchingLong(self, xseq, seq, ttt):
		# ^^ assumes xseq length self.n, seq length n+1. NOT recursive.
		newttt = set(ttt)
		for j in range(0, len(xseq)):
			# ^^ until last input to xseq, if any
			p, pp = seq[j], seq[j + 1]
			#  print "p, pp ==", p, pp
			edgeUsed = False
			for bb in range(0, self.b):
				if (p, pp, bb) in newttt:
					edgeUsed = True
					if bb != xseq[j]:
						# i.e., x[j] does not match previous value x[k]
						return
						# matchingString failed
						# COULD STILL MATCH JUST NOT UNIQUELY WHEN DOING
						# STRUCTURE FUNCTION, COMPARE WITH bTom
					break
			if not edgeUsed:
				newttt.add((p, pp, xseq[j]))
		# print xseq,"has uniquely accepting path",seq
		self.matches.add(xseq)
		# ^^ since matchingString did not fail above.

	def saveUniquelyAcceptingStateSeqs(self):
		# ^^ assumes complexityBounds has already been run,
		# so interestingSequences holds the data.
		pickle.dump(
			self.interestingSequences,
			open(
				"data/n = " + str(self.n) + ", q = " + str(self.q) + ".txt",
				"wb"
			)
		)


	def checkResetDeterminismFunction(self, seq, xseq):
		resetDeterministic = True
		for u in range(0, len(seq) - 1):
			for v in range(u + 1, len(seq) - 1):
				if (seq[u] == seq[v]) and (xseq[u] == xseq[v]) and (seq[u + 1] != seq[v + 1]) and (seq[u + 1] != 0) and (seq[v + 1] != 0):
					print "Reset determinism failed."
					return False
		return resetDeterministic

	def checkPropriety(self, seq):
		for u in range(0, len(seq)):
			for v in range(u + 1, len(seq)):
				if seq[u] == seq[v]:
					#found first loop...
					myLoop = [seq[t] for t in range(u, v+1)]
					for T in range(v, len(seq)): # now check each T >= v;
						somethingInMyLoopOccursStrictlyAfterT = False
						for S in range(T + 1, len(seq)):
							if seq[S] in myLoop: # does anything in myLoop occur strictly after T?
								somethingInMyLoopOccursStrictlyAfterT = True
						if not somethingInMyLoopOccursStrictlyAfterT:
							myExit = T # when NO, call that myExit = T.
							print "Propriety test returned " +str(seq[myExit] == seq[u]) + " due to u=" + str(u) + " and v=" + str(v) + " and T=" + str(T)
							return (seq[myExit] == seq[u]) # Is seq[myExit] == seq[u]? If NO, return False
		return True

	def checkRecurrent(self, seq):
		recurrent = True
		for qTest in range(1, self.q):#we don't require recurrence to the start state
			#change algorithm to: if qTest is visited between two visits to qOther then qTest is okay.
			foundRecurrence = False
			for u in range(0, len(seq)):
				for w in range(u, len(seq)):
					for v in range(w, len(seq)):
						if (seq[u] == seq[v]) and (u <= w) and (w <= v) and (u < v) and (seq[w] == qTest):
							foundRecurrence = True
			if (recurrent and not foundRecurrence):
				print "Recurrence failed"
				return False
		return recurrent

	def complexityBounds(self, seq, tt, usedAllStates, xseq, checkDeterminism,
		verbose=False, printStateSequences=False, printStats=False,
		endStateIsStartState=False
	):
		#print "complexityBounds says endSt.. is " + str(endStateIsStartState)
		#print "The truth value of printStateSequences is " + str(printStateSequences)
		if (self.isItTimeToReturn):
			return
		# ^^ CHANGE TO PASS IN MAX STATE USED
		# here xseq==() if we are finding all complexities;
		# otherwise len(xseq) should be n
		"""
		The local variable
			useFiles
		is equal to
			self.useFiles
		if we want to use any info that may have been loaded.
		Presumably that's faster,
		at some point (14,7) took 2.25 secs with True, 3.2 with False.
		"""
		useFiles = self.useFiles
		"""
		We enforce slow increase of seq:
		"""
		if not self.slowincrease(seq):
			return
		"""
		For long computations we periodically print a progress report:
		"""
		if len(seq) == self.q: #heuristically, just to not fill up the screen too much and too little
			nowTime = time.time()
			if nowTime - self.lastTime > 60:
				self.lastTime = nowTime
				print time.asctime( time.localtime(time.time()) ),
				#print "(" + str(round((nowTime - self.firstTime)/3600,2)) + " hours)",
				print seq
		transitionTable = tt.copy()
		deterministic = True
		"""
		Check whether seq is inconsistent with xseq so far; if so, return:
		[
			if doing PDAs,
			we need xseq to actually be at least a 18=3x2x3-ary string and use a list [] as a stack with .pop and .append
			which gets passed to complexityBounds().
			we need a boolean doingPDAs = True.
			the pseudocode is then:
			switch xseq[j]:
				case 0 : don't pop, read 0, don't push
				case  1: don't pop, read 0, push 0
				case  2: don't pop, read 0, push 1
				case  3: don't pop, read 1, don't push
				case  4: don't pop, read 1, push 0
				case  5: don't pop, read 1, push 1
				case  6: pop 0, read 0, don't push
				case  7: pop 0, read 0, push 0
				case  8: pop 0, read 0, push 1
				case  9: pop 0, read 1, don't push
				case 10: pop 0, read 1, push 0
				case 11: pop 0, read 1, push 1
				case 12: pop 1, read 0, don't push
				case 13: pop 1, read 0, push 0
				case 14: pop 1, read 0, push 1
				case 15: pop 1, read 1, don't push
				case 16: pop 1, read 1, push 0
				case 17: pop 1, read 1, push 1
		]
		"""
		if len(xseq) == self.n:
			if endStateIsStartState and len(seq) == self.n + 1:
				if seq[self.n] != seq[0]: # which by the way is 0...
					#print str(seq) + " had wrong end state"
					return False # or just return
				else:
					pass
					#print "."
			else:
				pass
				#print str(endStateIsStartState)
				#print str(seq)
				#print str(self.n + 1)
				#print
			newttt = set(self.emptyttt)
			for nn in range(0, len(seq) - 1):
				p, pp = seq[nn], seq[nn + 1]
				newttt.add(
					(p, pp, xseq[nn])
				)
				#if seq == (0, 0, 0, 0):
				#	print "newttt = " + str(newttt)
				transitionTable[p, pp] = sum([1 for bb in range(0, self.b) if (p, pp, bb) in newttt])

			#print "checking determinism" #could do this faster by checking determinism gradually as the state sequence seq is being built.
			if checkDeterminism:
				#print "check for determinism"
				for p in xrange(0, self.q):
					for pp in xrange(0, self.q):
						for ppp in xrange(0, self.q):
							for b in xrange(0, self.b):
								if (p, pp, b) in newttt and (p, ppp, b) in newttt and pp != ppp:
									deterministic = False
									if checkDeterminism: #new line 2015-12-25. should make it a bit faster since we don't matrix exponentiate unneededly
										return
									#print str(newttt) + " is not deterministic."
				#end of check for determinism
		"""
		j = max input to seq; also
		j is the length of any relevant xseq going through the states in seq
		"""
		j = len(seq) - 1
		if j > 0:
			if useFiles:
				if not usedAllStates:
					seqQ = min(len(seq), self.q - 1)
					# ^^ the max number of states that could possibly appear
					# in seq
					try:
						myBool = (seq in self.helperSeqs[j][seqQ])
					except:
						print "problem with seq=", seq, "j=", j, "seqQ=", seqQ
					if not myBool:
						return
				elif len(seq) < self.n + 1:
					seqQ = min(len(seq), self.q)
					# ^^ the max number of states that could possibly appear
					# in seq
					if not (seq in self.helperSeqs[j][seqQ]):
						return
			if (transitionTable[seq[j - 1], seq[j]] == 0):
				transitionTable[seq[j - 1], seq[j]] = 1
				# ^^ the last transition in seq
			if (not useFiles) or (j == self.n):
				# so we have no saved answer to the matrix computation...
				testNumber = numpy.linalg.matrix_power(  # 2015-06-17 this is where we count the no. of paths, not strings!
					transitionTable, j
				)[0, seq[j]]
				if (testNumber == 0) or (testNumber > self.bTom) or (checkDeterminism and not deterministic):
					#if seq == (0, 0, 0, 0):
					#	print "Number of paths " + str(testNumber) + " was more than " + str(self.bTom)
					#	print "for xseq=" + str(xseq) + " and m=" + str(self.m)
					return  # non-uniqueness was found.
				else:
					#if seq == (0, 0, 0, 0):
					#	print "Number of paths " + str(testNumber) + " was not more than " + str(self.bTom)
					#	print "for xseq=" + str(xseq) + " and m=" + str(self.m)
					#	print "for transitionTable = " + str(transitionTable)
					if j == len(xseq):
						self.edges[len(newttt)] += 1
						if verbose or printStats:#one kind of verbose...
							print ""
							print str(int(testNumber)) + " = # of paths matching, state sequence: " + str(seq) + ", edges[" + str(len(newttt)) + "] = " + str(self.edges[len(newttt)]) + "."
						if verbose or printStateSequences:#another kind of verbose, really
							print "\t\t" + str(seq) + "," # used to produce a file for Google App Engine for Structure Function Calculator
						if verbose:
							print 
						if (self.returnWhenFound):
							self.isItTimeToReturn = True
							return
						else:
							pass
							#print "returnWhenFound is " + str(self.returnWhenFound)
						if self.approxLength:
							myK = [0]*(j+1)
							#######print "How about nearby lengths?"
							for kkk in range(0, j+1):#2*j somewhat ad hoc
								#print kkk,
								#print numpy.linalg.matrix_power(  # 2015-06-17 this is where we count the no. of paths, not strings!
								#	transitionTable, kkk
								#)[0, seq[j]],
								myK[kkk] = numpy.linalg.matrix_power(  # 2015-06-17 this is where we count the no. of paths, not strings!
									transitionTable, kkk
								)[0, seq[j]]
							#excludedLengths = [1, 2, 3, 4, 5, 6, 7, 9, 10, 12, 13, 14, 15, 17, 18, 20, 21, 23, 25, 26] #OK
							#excludedLengths = [1, 2, 4, 7]
							#excludedLengths = [9, 10, 12, 13, 14, 15, 17, 18, 20, 21, 23, 25, 26]#not OK
							#excludedLengths = [4, 5, 6, 7, 9, 10, 12, 13, 14, 15, 17, 18, 20, 21, 23, 25, 26] #OK!
							#excludedLengths = [6, 7, 9, 10, 12, 13, 14, 15, 17, 18, 20, 21, 23, 25, 26]#not OK
							#excludedLengths = [5, 6, 7, 9, 10, 12, 13, 14, 15, 17, 18, 20, 21, 23, 25, 26]#OK. |F|=16
							#excludedLengths = [5, 6, 7, 9, 10, 12, 13, 14, 15, 17, 18, 20, 21, 23, 25]#OK
							#excludedLengths = [5, 6, 7, 9, 10, 12, 13, 14, 15, 17, 18, 20]#OK
							#excludedLengths = [5, 6, 7, 9, 10, 12, 13, 14, 15, 17]#not OK
							#excludedLengths = [3,7,9]
							excludedLengths = [1]
							if (sum([myK[u] for u in excludedLengths]) == 0):
								print str(myK)
								print str(seq)
						if self.checkResetDeterminism:
							if self.checkRecurrent(seq) and self.checkPropriety(seq):# and self.checkResetDeterminismFunction(seq, xseq):
								print "Eureka..."
		if j < self.n:
			#for p in range(0, self.q): #used until 2015-12-26, but the best solution is usually found later so let's go backwards:
			for p in xrange(self.q - 1, -1, -1):
				if p < self.q - 1:
					self.complexityBounds(
						seq + (p,),
						transitionTable,
						usedAllStates,
						xseq,
						checkDeterminism,
						verbose,
						printStateSequences=printStateSequences,
						printStats=printStats,
						endStateIsStartState=endStateIsStartState
					)
				else:
					self.complexityBounds(
						seq + (p,),
						transitionTable,
						True,
						xseq,
						checkDeterminism,
						verbose,
						printStateSequences=printStateSequences,
						printStats=printStats,
						endStateIsStartState=endStateIsStartState
					)
			return
		self.numInterestingSequences += 1
		self.interestingSequences.add(seq)
		if len(xseq) == 0:
			# in other words, we are not looking for a specific xseq
			self.checkMatching(xseq, seq, self.emptyttt, checkDeterminism)
			# ^^ [] is the empty list, which is not hashable,
			# so use the empty tuple () instead.
		if len(xseq) == self.n:
			# self.checkMatchingLong(xseq, seq, self.emptyttt)
			# ^^ this actually seems superfluous now; if transitionTable has
			# few enough paths then some way to fit xseq exists
			# so instead just do this:
			self.matches.add(xseq)
			# and I guess this:
			self.matchesPairs.add((str(transitionTable), xseq))
		"""
		End of def complexityBounds
		"""
	"""
	End of class SimpleStrings
	"""
